import UIKit

class GameViewController: UIViewController {
    
    
    @IBOutlet weak var lblEventMessage: UILabel!
    @IBOutlet weak var lblCurrentLocationVal: UILabel!
    @IBOutlet weak var southButtonObject: UIButton!
    @IBOutlet weak var northButtonObject: UIButton!
    @IBOutlet weak var eastButtonObject: UIButton!
    @IBOutlet weak var westButtonObject: UIButton!
    @IBOutlet weak var lblCustomEvent: UILabel!
    
    var model: GameModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        model = GameModel()
        lblEventMessage.isHidden = true
        lblCustomEvent.isHidden = true
        lblCurrentLocationVal.text = "( X:0, Y:0 )"
        
    }
    
    @IBAction func southButton(_ sender: UIButton) {
        model.move(direction: Direction.south)
        lblEventMessage.text = "Moved South"
        updateView()
    }
    @IBAction func northButton(_ sender: UIButton) {
        model.move(direction: Direction.north)
        lblEventMessage.text = "Moved North"
        updateView()
    }
    @IBAction func eastButton(_ sender: UIButton) {
        model.move(direction: Direction.east)
        lblEventMessage.text = "Moved East"
        updateView()
    }
    @IBAction func westButton(_ sender: UIButton) {
        model.move(direction: Direction.west)
        lblEventMessage.text = "Moved West"
        updateView()
    }
    @IBAction func restartButton(_ sender: UIButton) {
        model.restart()
        lblEventMessage.text = ""
        updateView()
    }
    
    func updateView(){
        
        lblEventMessage.isHidden = false

        let currentLocationX = model.currentLocation().coordinate.x
        let currentLocationY = model.currentLocation().coordinate.y
        
        lblCurrentLocationVal.text = "( X: \(currentLocationX) , Y: \(currentLocationY) )"
        
        //Disable Buttons if needed
        let directionAllowed = model.currentLocation().allowedDirections
        if directionAllowed.contains(.east){
            eastButtonObject.isEnabled = true
        }
        else {eastButtonObject.isEnabled = false}
        if directionAllowed.contains(.west){
            westButtonObject.isEnabled = true
        }
        else {westButtonObject.isEnabled = false}
        if directionAllowed.contains(.north){
            northButtonObject.isEnabled = true
        }
        else {northButtonObject.isEnabled = false}
        if directionAllowed.contains(.south){
            southButtonObject.isEnabled = true
        }
        else {southButtonObject.isEnabled = false}
        
        //check for event
        let event = model.currentLocation().event
        if event != "" {
            lblCustomEvent.isHidden = false
            lblCustomEvent.text = event
        }
        else {
            lblCustomEvent.text = ""
            lblCustomEvent.isHidden = true
        }
    }
}
