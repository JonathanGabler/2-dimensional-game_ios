import Foundation

enum Direction {
    case north, east, west, south
}

struct Coordinate {
    let x: Int
    let y: Int
}

struct Location {
    let coordinate: Coordinate
    let allowedDirections: [Direction]
    let event: String?
}

struct Row {
    let locations: [Location]
}

class GameModel {
    private var grid = [Row]()
    
    private var currentRowIndex = 0          // start at (x: 0, y: 0)
    private var currentLocationIndex = 0
    
    private let minXYvalue = -2
    private let maxXYvalue = 2
    
    init() {
        self.grid = createGameGrid()
    }
}

// MARK: - Public facing methods
extension GameModel {
    func restart() {
        currentRowIndex = 0
        currentLocationIndex = 0
    }
    
    func currentLocation() -> Location {
        let coordinate = Coordinate(x: currentRowIndex, y: currentLocationIndex)
        let directionsAllowed = allowedDirections(forCoordinate: coordinate)
        print(directionsAllowed)
        let allowedDirections = directionsAllowed
        print(allowedDirections)
        if allowedDirections.contains(.east){
            print("True")
        }
        let eventMessage: String? = event(forCoordinate: coordinate)

        return Location(coordinate: coordinate, allowedDirections: directionsAllowed, event: eventMessage)
    }
    
    func move(direction: Direction) {
        switch direction{
        case.west: currentRowIndex-=1
        case.east: currentRowIndex+=1
        case.north: currentLocationIndex+=1
        case.south: currentLocationIndex-=1
        }
    }
}

// MARK: - Helper methods for creating grid
extension GameModel {
    private func createGameGrid() -> [Row] {
        let possibleXYValues = Array(minXYvalue...maxXYvalue)
        let gameGrid: [Row] = possibleXYValues.map { yValue in
            let locations: [Location] = possibleXYValues.map { xValue in
                let coordinate = Coordinate(x: xValue, y: yValue)
                let allowedDirections = self.allowedDirections(forCoordinate: coordinate)
                let event = self.event(forCoordinate: coordinate)
                
                return Location(coordinate: coordinate, allowedDirections: allowedDirections, event: event)
            }
            
            return Row(locations: locations)
        }
        
        return gameGrid
    }
    
    private func allowedDirections(forCoordinate coordinate: Coordinate) -> [Direction] {
        var directions = [Direction]()
        
        switch coordinate.y {
        case minXYvalue: directions += [.north]
        case maxXYvalue: directions += [.south]
        default: directions += [.north, .south]
        }
        
        switch coordinate.x {
        case minXYvalue: directions += [.east]
        case maxXYvalue: directions += [.west]
        default: directions += [.east, .west]
        }
        
        return directions
    }
    
    private func event(forCoordinate coordinate: Coordinate) -> String? {
        var message:String
        let xValue = coordinate.x
        let yValue = coordinate.y
        if xValue == 1 && yValue == 2 {
            message = "You got a 🏠 yay debt!"
        }
        else {
            message = ""
        }
        return message
    }
}
